# window.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Adw, GObject, Gio, Gdk
from master_key import Timer, Database, Password, Dialog, backup
from gettext import gettext as _
from zxcvbn import zxcvbn
import time


class WindowState:
    LOGIN, SETUP, EMPTY, LIST = range(4)


@Gtk.Template(resource_path='/com/gitlab/guillermop/MasterKey/ui/window.ui')
class Window(Adw.ApplicationWindow):
    __gtype_name__ = 'Window'

    state = GObject.Property(type=int)
    empty = GObject.Property(type=bool, default=True)

    menu_button = Gtk.Template.Child()
    main_stack = Gtk.Template.Child()
    password_entry = Gtk.Template.Child()
    setup_entry = Gtk.Template.Child()
    setup_button = Gtk.Template.Child()
    unlock_button = Gtk.Template.Child()
    confirm_entry = Gtk.Template.Child()
    level_bar = Gtk.Template.Child()
    list_stack = Gtk.Template.Child()
    search_bar = Gtk.Template.Child()
    search_entry = Gtk.Template.Child()
    search_button = Gtk.Template.Child()
    add_button = Gtk.Template.Child()
    list_box = Gtk.Template.Child()
    error_label = Gtk.Template.Child()
    notification = None
    overlay = Gtk.Template.Child()
    search_controller = Gtk.Template.Child()
    last_search_results: int = 0

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setup_actions()
        self.bind_properties()
        db = Database.get_default()
        db.connect('connected', self.on_database_connected)
        self._toRemove = []

    def bind_properties(self):
        app = self.get_application()
        app.bind_locked(self.search_button, 'visible')
        app.bind_locked(self.add_button, 'visible')

    def setup_actions(self):
        self.add_action('add', self.add_password)
        self.add_action('search', self.search, True)
        self.add_action("import", self.on_import)
        self.add_action("export", self.on_export, True)
        self.add_action("undo", self.undo)

    def add_action(self, key, callback, bind=False):
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        self.get_application().bind_locked(action, 'enabled')
        if bind:
            self.bind_property(
                "empty",
                action,
                "enabled",
                GObject.BindingFlags.INVERT_BOOLEAN | GObject.BindingFlags.SYNC_CREATE
            )
        super().add_action(action)

    @Gtk.Template.Callback()
    def on_state_changed(self, *args):
        self.search_bar.set_key_capture_widget(None)
        state = self.props.state
        if state == WindowState.LOGIN:
            visible = 'login'
            self.search_bar.set_search_mode(False)
            self.password_entry.set_sensitive(True)
            self.password_entry.grab_focus()
            if self.notification:
                self.notification.delete()
            self.set_default_widget(self.unlock_button)
        elif state == WindowState.SETUP:
            self.search_bar.set_search_mode(False)
            self.setup_entry.set_sensitive(True)
            self.setup_entry.grab_focus()
            if self.notification:
                self.notification.delete()
            self.set_default_widget(self.setup_button)
            visible = 'setup'
        elif state == WindowState.EMPTY:
            self.search_bar.set_search_mode(False)
            self.list_stack.set_visible_child_name('list')
            self.props.empty = True
            visible = 'empty'
        else:
            self.props.empty = False
            visible = 'list'
            self.search_bar.set_key_capture_widget(self)
        self.main_stack.set_visible_child_name(visible)

    @Gtk.Template.Callback()
    def on_items_changed(self, *args):
        if self.get_application().props.is_locked:
            return
        if self.list_box.get_n_items() == 0:
            self.props.state = WindowState.EMPTY
            return
        self.props.state = WindowState.LIST
        if self.list_box.get_n_visible_items() == 0:
            self.list_stack.set_visible_child_name('empty')
        else:
            self.list_stack.set_visible_child_name('list')

    def add_password(self, *args):
        dialog = Dialog(self)

        def on_response(d, response):
            if response == Gtk.ResponseType.OK:
                password = Password.create(*dialog.get_data())
                self.list_box.insert(password)
                index = self.list_box.get_n_items() - 1
                self.list_box.get_row_at_index(index).activate()
            dialog.destroy()
        dialog.connect('response', on_response)
        dialog.present()

    def search(self, *args):
        mode = self.search_bar.get_search_mode()
        if mode and not self.search_controller.contains_focus():
            self.search_entry.grab_focus()
        else:
            self.search_bar.set_search_mode(not mode)

    def undo(self, *args):
        for temp in reversed(self._toRemove):
            index = list(temp)[0]
            password = temp[index]
            self.list_box.insert(password, index)
        self._toRemove = []

    @Gtk.Template.Callback()
    def on_search_mode_changed(self, *args):
        if not self.search_bar.get_search_mode():
            self.search_button.grab_focus()

    @Gtk.Template.Callback()
    @Timer.report_activity
    def on_search_changed(self, *args):
        self.list_box.collapse_all()

    def delete_password(self, password):
        self.list_box.collapse_all()
        index = self.list_box.remove(password)
        self._toRemove.append({index: password})

        message = '"{}" {}'.format(password.domain, _("deleted"))
        if len(self._toRemove) > 1:
            message = '{0} passwords deleted'.format(len(self._toRemove))

        if self.notification:
            self.notification.set_title(message)
            return

        self.notification = Adw.Toast(
            title=message,
            button_label=_('Undo'),
            action_name='win.undo',
            priority=Adw.ToastPriority.HIGH
        )

        def on_dismissed(*args):
            self.notification = None;
            if self._toRemove:
                for temp in self._toRemove:
                    index = next(iter(temp))
                    temp[index].delete()
                self._toRemove = {}

        self.notification.connect('dismissed', on_dismissed)
        self.overlay.add_toast(self.notification)

    @Gtk.Template.Callback()
    def on_unlock_button_clicked(self, entry):
        button = self.get_default_widget()
        button.grab_focus()
        button.set_sensitive(False)
        Database.get_default().unlock(entry.get_text())

    def on_database_connected(self, db, success):
        self.unlock_button.set_sensitive(True)
        self.setup_button.set_sensitive(True)
        if success:
            self.get_application().is_locked = False
            self.setup_entry.set_text('')
            self.confirm_entry.set_text('')
            self.password_entry.set_text('')
            # maybe there's an easier way
            self.setup_entry.get_first_child().set_visibility(False)
            self.confirm_entry.get_first_child().set_visibility(False)
            self.password_entry.get_first_child().set_visibility(False)
            if self.notification:
                self.notification.delete()
            self.list_box.load()
            self.password_entry.get_style_context().remove_class("error")
            self.error_label.set_visible(False)
        else:
            self.password_entry.grab_focus()
            self.password_entry.get_style_context().add_class("error")
            self.error_label.set_visible(True)

    def validate_setup(self):
        confirm = self.setup_entry.get_text() == self.confirm_entry.get_text()
        self.setup_button.set_sensitive(
            confirm and self.level_bar.get_value() > 0)

    @Gtk.Template.Callback()
    def on_setup_entry_changed(self, entry):
        password = entry.get_text()
        score = 0
        if password:
            results = zxcvbn(password)
            score = results.get("score", 0) + 1
        self.level_bar.set_value(score)
        self.validate_setup()

    @Gtk.Template.Callback()
    def on_confirm_entry_changed(self, *args):
        self.validate_setup()

    @Timer.report_activity
    def on_import(self, *args):
        file_chooser = Gtk.FileChooserNative(
            modal = True,
            transient_for=self,
            action=Gtk.FileChooserAction.OPEN
        )
        filter_json = Gtk.FileFilter()
        filter_json.set_name(_("JSON files"))
        filter_json.add_mime_type("application/json")
        file_chooser.add_filter(filter_json)

        def on_response(d, response):
            if response == Gtk.ResponseType.ACCEPT:
                filename = file_chooser.get_file().get_path()
                try:
                    passwords = backup.import_passwords(filename)
                except Exception:
                    self.overlay.add_toast(Adw.Toast(
                        title=_('Failed to restore passwords')
                    ))
                else:
                    self.list_box.append(*passwords)
            file_chooser.destroy()

        file_chooser.connect('response', on_response)
        file_chooser.show()

    @Timer.report_activity
    def on_export(self, *args):
        file_chooser = Gtk.FileChooserNative(
            modal = True,
            transient_for=self,
            action=Gtk.FileChooserAction.SAVE
        )
        filter_json = Gtk.FileFilter()
        filter_json.set_name(_("JSON files"))
        filter_json.add_mime_type("application/json")
        file_chooser.set_current_name(f'{time.strftime("%Y%m%d-%H%M%S")}.json')
        file_chooser.add_filter(filter_json)

        def on_response(d, response):
            if response == Gtk.ResponseType.ACCEPT:
                filename = file_chooser.get_file().get_path()
                backup.export_passwords(filename)
            file_chooser.destroy()

        file_chooser.connect('response', on_response)
        file_chooser.show()

    @Gtk.Template.Callback()
    def on_close_request(self, *args):
        if self.notification:
            self.notification.dismiss()
        return False

    @Gtk.Template.Callback()
    def on_clicked(self, *args):
        if not self.get_application().props.is_locked:
            Timer.get_default().reset()

    @Gtk.Template.Callback()
    def on_key_pressed(self, *args):
        if not self.get_application().props.is_locked:
            Timer.get_default().reset()

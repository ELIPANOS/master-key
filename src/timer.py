# timer.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import GLib, GObject
from functools import wraps
from master_key import Settings, logger


class Timer(GObject.GObject):

    __gtype_name__ = 'Timer'
    __gsignals__ = {
        'timeout': (GObject.SIGNAL_RUN_FIRST, None, ())
    }

    instance: 'Timer' = None
    timeout_id: int = 0

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @staticmethod
    def get_default() -> 'Timer':
        if Timer.instance is None:
            Timer.instance = Timer()
        return Timer.instance

    def stop(self):
        if self.timeout_id == 0:
            return
        GLib.Source.remove(self.timeout_id)
        self.timeout_id = 0

    def reset(self):
        self.stop()
        self.start()

    def start(self, *args):
        def timeout(*_):
            self.timeout_id = 0
            self.emit('timeout')
            return False

        minutes = Settings.get_default().lock_timeout * 60
        self.timeout_id = GLib.timeout_add_seconds(minutes, timeout)

    @staticmethod
    def report_activity(func):
        @wraps(func)
        def wrapper(*args):
            Timer.get_default().reset()
            logger.debug(f'activity in {func}')
            return func(*args)
        return wrapper


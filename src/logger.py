# logger.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

DEBUG = logging.DEBUG
ERROR = logging.ERROR
FORMAT = '[%(levelname)-s] %(asctime)s %(message)s'

logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter = logging.Formatter(FORMAT)
handler.setFormatter(formatter)
logger.setLevel(ERROR)
logger.addHandler(handler)


def info(msg: str):
    logger.info(msg)


def debug(msg: str):
    logger.debug(msg)


def error(msg: str):
    logger.error(msg)


__all__ = ['DEBUG', 'ERROR', 'debug', 'error']
